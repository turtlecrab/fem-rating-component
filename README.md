# Frontend Mentor - Interactive rating component

<https://fem-rating-component-seven.vercel.app/>

My second project for Frontend Mentor.

I used:
- vanilla JS
- SCSS as a preprocessor(first time using it)
- CSS animations(first time using it too)
- BEM naming convention
- Parcel as a bundler
