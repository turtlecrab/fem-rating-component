const surveyForm = document.getElementById('survey')
const radios = document.querySelector('.card__radios')
const submitButton = document.querySelector('.card__submit')
const star = document.querySelector('.card__star')

const ratingPage = document.querySelector('#rating-page')
const thankyouPage = document.querySelector('#thankyou-page')
const thankyouRatingSpan = document.getElementById('rating')

surveyForm.addEventListener('submit', sendRating)

function sendRating(e) {
  e.preventDefault()
  const rating = e.target.rate.value
  
  if (!rating) {
    radios.classList.remove('card__radios--alert')
    radios.offsetWidth //wtf
    radios.classList.add('card__radios--alert')
    return
  }
  
  submitButton.disabled = true
  star.classList.add('card__star--spin')

  setTimeout(() => resolve(e.target.rate.value), 1000)
}

function resolve(rating) {
  ratingPage.style.display = 'none'
  thankyouRatingSpan.textContent = rating
  thankyouPage.style.display = 'block'
}